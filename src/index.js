import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { HackerProvider } from "./HackerContextapi/HackerContextapi";
import App from './App';
import * as serviceWorker from './serviceWorker';
import {ProductProvider} from './ContextApi/ContextApi'

ReactDOM.render(<HackerProvider><ProductProvider><App /></ProductProvider></HackerProvider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
