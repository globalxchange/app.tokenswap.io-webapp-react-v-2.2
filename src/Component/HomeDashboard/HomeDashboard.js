import React, { useState,useEffect,useContext } from "react";
import Logo from '../../Image/LandingLogoWhitemode.svg'
import cutombtext from '../../Image/whitemodeexhange.png'

import darkmodelogo from '../../Image/darkmodelogo.png'
import exhangelhackerdark from '../../Image/exhangelhackerdark.png'
import Compass from '../../Image/Compaus.png'
import Compare from '../../Image/compare.png'
import Counter from '../../Image/counter.png'
import {useSwipeable} from 'react-swipeable'
import { Hacker } from "../../HackerContextapi/HackerContextapi";
import './HomeDashboard.scss'
export default function HomeDashboard() {

    const Info=[
        {
            cardId:0,
            name:"Compare",
            des:"As a protocol, Terminals.com s a liquidity & data aggregator for exchanges. Its is comprised of several micro-services which include",
            img:Compare
        },
        {
            cardId:1,
            name:"Compass",
            des:"As a protocol, Terminals.com s a liquidity & data aggregator for exchanges. Its is comprised of several micro-services which include",
       img:Compass
        },
        {
            cardId:2,
            name:"Counter",
            des:"As a protocol, Terminals.com s a liquidity & data aggregator for exchanges. Its is comprised of several micro-services which include",
img:Counter
        }
    ]

    const handlers = useSwipeable({
        onSwiped: (e) =>handleSwipe(e.dir.toLowerCase())
    })
    const [selectedId, setSelectedId] = useState("1");
    const [carouselData, setCarouselData] = useState([{ title: "" }]);
    const [selectedCard, setSelectedCard] = React.useState({
      count: 0,
      info: null,
    });
    const hacker = useContext(Hacker);
  const { togglemode,mode } = hacker;
    const handleSwipe = (dir )=>{
      console.log("dir", dir);
      if(dir==="right"){
        console.log("Rdir", dir);
        if(selectedCard.count === 0){
        //   let data = videoData[videoData.length - 1]
        //   setSelectedCard({count: videoData.length - 1, info: data});
          return;
        }
        let temp = Info[selectedCard.count - 1];
        setSelectedCard({count: selectedCard.count - 1, info: temp})
  
      }else{
        console.log("Ldir", dir);
        if(selectedCard.count === Info.length - 1){
          let data = Info[0]
          setSelectedCard({count: 0, info: data});
          return;
        }
        let temp = Info[selectedCard.count + 1];
        setSelectedCard({count: selectedCard.count + 1, info: temp})
  
      }
  
    }
  
    const getClassName = (num) => {
      const { count } = selectedCard;
      if (count === Info.length - 1 && num === 0) {
        return "next-card";
      } else if (count === 0 && num === Info.length - 1) {
        return "previous-card";
      } else {
        if (count === num) {
          return "current-card";
        } else if (count - 1 === num) {
          return "previous-card";
        } else if (count + 1 === num) {
          return "next-card";
        } else {
          return "hidden";
        }
      }
    };
    useEffect(() => {
        setSelectedCard({ count: 0, info: Info[0] });
    }, []);
    return (
        <div className="homepage">

  <img style={{cursor:"pointer"}} src={mode?darkmodelogo:Logo} alt="" onClick={togglemode}/>
  <img className="exchange" src={mode?exhangelhackerdark:cutombtext} alt=""/>

        <div {...handlers} className="HomeCarasoule w-100 d-flex">
        <div className="cards-wrapper d-flex align-items-center">
          <div className="cards-wrapper position-relative">
            {Info.map((obj, num) => (
              <label
                key={obj.cardId}
                className={`position-absolute d-flex justify-content-center align-items-center ${getClassName(num)}`}
                onClick={() => {
                  let a = setTimeout(() => {selectedCard.count === num
                    ? console.log()
                    : setSelectedCard({ count: num, info: obj });
                    clearTimeout(a)
                    
                  }, 150);
                  
                }}
              >
                <div className="w-100 below-text d-flex">
                    <div className="leftside">
                    <h5>{obj.name}</h5>
           <p>{obj.des}</p>
            
                    </div>
          <div className="rightside" style={{backgroundImage: `url(${obj.img})`}}>

          </div>
                </div>
              </label>
            ))}
          </div>
        </div>
      </div>
      </div>
    )
}
