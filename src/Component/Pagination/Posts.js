import React from 'react'
import { Table } from 'react-bootstrap';
import './Posts.css'
const Posts = ({ posts, loading }) => {
  if (loading) {
    return <h2>Loading...</h2>;
  }
  
 const renderchange=(percent)=>{
if(percent>0)
{
  return <span style={{color:"green"}}>{percent}% &uarr;</span>
}
else if(percent<0) 
{
  return <span style={{color:"red"}}>{percent}% &darr;</span>
}
  }

  return (
 <>
<div className="containe row">

<Table className="post-table"responsive>
               <thead>
                 <tr className="list-tr" >
                   <th>CURRENCY</th>
                   <th>MARKET PAIR</th>
   
                   <th>BASE VOLUME(24)</th>
                   <th>HIGH </th>
                   <th>LOW</th>
                   <th>PRICE</th>
                   <th>CHANGE</th>
                 </tr>
               </thead>
               <tbody>

{posts.map(post => {
const CHANGEPCT24HOUR=(Math.round(post.RAW.USD.CHANGEPCT24HOUR*100)/100)
const HIGHDAY=(Math.round(post.RAW.USD.HIGHDAY*100)/100)
const LOWDAY=(Math.round(post.RAW.USD.LOWDAY*100)/100)
const PRICE=(Math.round(post.RAW.USD.PRICE*100)/100)
const OPEN24HOUR=(Math.round(post.RAW.USD.OPEN24HOUR*100)/100)


return(
<tr >
<td><img style={{
height:"30px",paddingRight:"6px"
}}src={`http://www.cryptocompare.com/${post.CoinInfo.ImageUrl}`} alt={post.CoinInfo.FullName}/>{post.CoinInfo.FullName}</td>
<td>{post.CoinInfo.Internal+"/"+post.RAW.USD.TOSYMBOL}</td>
<td>{OPEN24HOUR}</td>
<td>{HIGHDAY}</td>

<td>{LOWDAY}</td>
<td>{PRICE}</td>
<td>{renderchange(CHANGEPCT24HOUR)}</td>
</tr>
)

})}
</tbody>
</Table>
</div>
</>
)
}
export default Posts