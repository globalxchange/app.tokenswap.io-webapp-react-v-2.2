import React, { Component } from 'react'
import { ProductConsumer } from '../../ContextApi/ContextApi'
import './Compare.css'
import Modal from 'react-modal';
const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};
const options = ["Select a base Cryptocurrency", "First Option", "Second Option", "Third Option"]
export default class Compare extends Component {

    constructor(props) {
        super(props);
        
        this.state = { value: 'select'};
      }
      onChange(e) {
        this.setState({
          value: e.target.value
        })
      }
     
      
     
  render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { items, inputValue, Data, modalIsOpen, openModal, closeModal, handSub, check,count} = value;

                    return (

                        <>
                        <div className="Compare-page">
                        <div className="row Compare-style">
                            <div>
                                <h1 className="compare-text">Compare</h1>
                                <p className="Exchange-select">SELECT EXCHANGE</p>
                            </div>
                            <div className="main_model">
                            
                                <Modal
                                    isOpen={modalIsOpen}
                                    onRequestClose={closeModal}
                                    style={customStyles}
                                    contentLabel="Example Modal"
                                >
                                    <div style={{ textAlign: "center" }}>
                                        <h1 className="pt-3 pb-4">Exchange</h1>
                                        <button className="Close-btn"onClick={closeModal}><i class="fas fa-times"></i></button>
                                        
                                    </div>
                                    <form className="form-Main" onSubmit={handSub}>
                                        {Data.map(item =>
                                       
                                            <div className=" col-sm-4  items-card-space">
                                                <div>
                                                    <label >
                                                        <input type="checkbox" name={check} onChange={inputValue.bind(this, item)} />
                                                        <div className="label-content ">
                                                            <div className="lable-conten-text">
                                                            <img className="model-image" src={item.images} />
                                                            <h1 className="model-Name">{item.Name}</h1>
                                                            </div>

                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        )}
                                       
                                       
                                      
                                    </form>
                                    {/*<button onSubmit={handSub} type="submit">submit</button> */}
                                    <div className="footer-models">
                                    <h5 className="footer-Text"onClick={closeModal}>CANCEL</h5>
                                        <h5 className="footer-Text-h5"onClick={closeModal}>COMPARE {count} EXCHANGES</h5>
                                        </div>
                                </Modal>
                                
                                {value.selected.map(i => {
                                    return (
                                        <>
                                            <div className=" items-card-spaces">
                                            <div >
                                                    <label className="label-content1 ">
                                                        <div className="gallery-text">
                                                    <h3><i className="fas fa-minus-square Minus-style"></i></h3>
                                                    </div>
                                                        <input type="checkbox" name={check} onChange={inputValue.bind(this, i)} />
                                            <div>
                                               <img className="model-image"src={i.images} />
                                               <i class="fas fa-minus-circle"></i>
                                                <h1 className="model-Name">{i.Name}</h1>
                                                </div>
                                                </label>
                                                </div> 
                                               



                                            </div>

                                        </>
                                    )
                                })}
                            <div className=" items-card-spaces">
                            <button className="btn-plus" onClick={openModal}><i class="fas fa-plus-square plus-style"></i></button>
                            </div>
                            </div>
                            </div>
                            
                        <div className="  serach-coin-selection" >
                            <div className="">
                            <p className="market-pair">SELECT MARKET PAIR</p>
                        <div className="form-group f-g">
       
        <select disabled="disabled" value={this.state.value} onChange={this.onChange.bind(this)} className="form-control selected-style">
          <option value="select">Select a quote Cryptocurrency</option>
          <option value="First">First</option>
          <option value="Second">Second</option>
          <option value="Third">Third</option>
        </select>
        <select  disabled="disabled" value={this.state.value} onChange={this.onChange.bind(this)} className="form-control selected-style1 selected-style">
        {options.map(option => {
          return <option value={option} key={option} >{option}</option>
        })}
      </select>
      </div>
      </div>
                        </div>
                        </div>
                        </>
                    );
                })}

            </ProductConsumer>
        )
    }
}