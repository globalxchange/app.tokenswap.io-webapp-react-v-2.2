import React, { Component } from 'react'
import {Nav, Navbar} from 'react-bootstrap';
import './Navigationbar.css'
import { Link } from 'react-router-dom';
export default class Navigationbar extends Component {
    render() {
        return (
            <div className="navbars">
     <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
  <Navbar.Brand ><a href="/">ExchangeHacker</a></Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto" >
      {/* <Nav.Link href="#features">Features</Nav.Link>
      <Nav.Link href="#pricing">Pricing</Nav.Link> */}
  
    </Nav>
    <Nav  variant="pills"  defaultActiveKey="/">
    <Nav.Link eventKey="link-1" ><Link to="/">Home</Link></Nav.Link>
    <Nav.Link eventKey="link-2"> <Link to="/Exchange/Bibox">Exchange</Link></Nav.Link>
      <Nav.Link eventKey="link-3"className="compare-position"><Link to="/Compare"><i className="fas fa-door-open"></i>
      Compare</Link>
      <div className="divcover"></div>
      </Nav.Link>
      {/* <Nav.Link eventKey={2} href="#memes">
        Dank memes
      </Nav.Link> */}
      
    </Nav>
  </Navbar.Collapse>
</Navbar>
            </div>
        )
    }
}
