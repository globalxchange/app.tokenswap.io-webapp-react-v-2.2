import React, { useState, useEffect } from 'react';
import Posts from '../Component/Pagination/Posts';
import Pagination from '../Component/Pagination/Pagination';
import axios from 'axios';

import './Coins.css';

const Coins = () => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(10);
  
  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);
      const res = await axios.get('https://min-api.cryptocompare.com/data/top/totalvolfull?limit=100&tsym=USD');
      setPosts(res.data.Data);
      setLoading(false);
    };

    fetchPosts();
  }, []);

 
  // Get current posts
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

  // Change page
  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <div className='mt-5'> 
      <Posts posts={currentPosts} loading={loading} />
      <Pagination
        postsPerPage={postsPerPage}
        totalPosts={posts.length}
        paginate={paginate}
      />
    </div>
  );
};

export default Coins;