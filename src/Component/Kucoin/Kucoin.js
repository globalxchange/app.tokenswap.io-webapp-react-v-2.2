import React, { Component } from 'react'
import InfiniteCarousel from 'react-leaf-carousel';
import { Button, ButtonToolbar } from 'react-bootstrap';
import { ProductConsumer } from '../../ContextApi/ContextApi'
import bibox from '../../Image/bibox.png'
import binance from '../../Image/binance.png'
import bitfinex from '../../Image/bitfinex.png'
import bittrex from '../../Image/bittrex.png'
import coss from '../../Image/coss.png'
import kraken from '../../Image/kraken.png'
import kucoin from '../../Image/kucoin.png'
import poloniex from '../../Image/poloniex.png'
import upbit from '../../Image/upbit.png'
import Coins from '../Coins'
import { Link } from 'react-router-dom'
export default class Kucoin extends Component {
    state = {
    }

    render() {
        const renderchange = (percent) => {
            if (percent > 0) {
                return <span style={{ color: "green" }}>{percent}% &uarr;</span>
            }
            else if (percent < 0) {
                return <span style={{ color: "red" }}>{percent}% &darr;</span>
            }
        }

        return (
            <ProductConsumer>
                {((value) => {
                    const { items, } = value;

                    return (
                        <>
                            <div className="imageslider_section">
                                <InfiniteCarousel
                                    breakpoints={[
                                        {
                                            breakpoint: 500,
                                            settings: {
                                                slidesToShow: 2,
                                                slidesToScroll: 2,
                                            },
                                        },
                                        {
                                            breakpoint: 850,
                                            settings: {
                                                slidesToShow: 3,
                                                slidesToScroll: 3,
                                            },
                                        },
                                        {
                                            breakpoint: 1024,
                                            settings: {
                                                slidesToShow: 4,
                                                slidesToScroll: 4,
                                            },
                                        },

                                    ]}
                                    dots={true}
                                    showSides={true}
                                    sidesOpacity={.5}
                                    sideSize={.1}
                                    slidesToScroll={6}
                                    slidesToShow={6}
                                    scrollOnDevice={true}
                                >

                                    <Link to="/Exchange/Bibox">
                                        <div className="sectionImage " >

                                            <img className="coinImg "
                                                alt=''
                                                src={bibox}
                                            />
                                            <p>Bibox</p>
                                        </div>
                                    </Link>

                                    <Link to="/Exchange/Binance">
                                        <div className="sectionImage">
                                            <img className="coinImg"
                                                alt=''
                                                src={binance}
                                            />
                                            <p>Binance</p>
                                        </div>
                                    </Link>

                                    <Link to="/Exchange/Bittrex">
                                        <div className="sectionImage ">
                                            <img className="coinImg"
                                                alt=''
                                                src={bittrex}
                                            />
                                            <p>Bittrex</p>
                                        </div>
                                    </Link>
                                    <Link to="/Exchange/Bitfinex">
                                        <div className="sectionImage ">
                                            <img className="coinImg"
                                                alt=''
                                                src={bitfinex}
                                            />
                                            <p>Bitfinex</p>
                                        </div>
                                    </Link>

                                    <Link to="/Exchange/Coss">
                                        <div className="sectionImage ">
                                            <img className="coinImg"
                                                alt=''
                                                src={coss}
                                            />
                                            <p>Coss</p>
                                        </div>
                                    </Link>
                                    <Link to="/Exchange/Kraken">
                                        <div className="sectionImage">
                                            <img className="coinImg"
                                                alt=''
                                                src={kraken}
                                            />
                                            <p>Kraken</p>
                                        </div>
                                    </Link>
                                    <Link to="/Exchange/Kucoin">
                                        <div className="sectionImage kuc">
                                            <img className="coinImg"
                                                alt=''
                                                src={kucoin}
                                            />
                                            <p>Kucoin</p>
                                        </div>
                                    </Link>
                                    <Link to="/Exchange/Poloniex">
                                        <div className="sectionImage">
                                            <img className="coinImg"
                                                alt=''
                                                src={poloniex}
                                            />
                                            <p>Poloniex</p>

                                        </div>
                                    </Link>
                                    <Link to="/Exchange/Upbit">
                                    <div className="sectionImage">
                                        <img className="coinImg"
                                            alt=''
                                            src={upbit}
                                        />
                                        <p>Upbit</p>
                                    </div>
                                    </Link>

                                </InfiniteCarousel>
                            </div>

                            <div className="Coininformation mt-3 pt-3">
                                <div className="row">
                                    <div className="col-md-6 Main_Title">
                                        <div className="Image_Tittle">
                                            <img className="c_image" src={kucoin} alt="Kucoin" />
                                            <h1>Kucoin </h1>
                                        </div>
                                        <div className="Coin_detail">
                                            <p className="pb-3 pt-3">
                                            KCS is an Ethereum-based ERC20 token issued by the Kucoin Cryptocurrency Exchange. The token holders benefit from bonuses (50% of the total trading fees charged by the platform), trading fee discounts, and other special services. Kucoin Cryptocurrency Exchange will buy back 100 million of the 200 million total tokens issued and burned them.
                                                                      </p>
                                        </div>
                                    </div>
                                    <div className="col-md-6 ">
                                        <div className="Coin_Types_selcetion">
                                            <div className="coin-type">
                                                <h5>TYPE</h5>
                                                <p style={{ fontSize: "12px" }}>Centralized</p>
                                            </div>
                                            <div className="coin-Email">
                                                <h5>SUPPORT EMAIL</h5>
                                                <p style={{ fontSize: "12px" }} className="Email-suport">https://kucoin.zendesk.com/hc/en-us</p>
                                            </div>
                                            <div className="coin-Country">
                                                <h5>COUNTRY</h5>
                                                <p style={{ fontSize: "12px" }}>Wan Chai, Hong Kong</p>
                                            </div>
                                        </div>
                                        <div className="WebSite_detail">
                                            <div className="website">
                                                <a href="https://www.kucoin.com/" target="_blank" >
                                                    <h5 className="hover_section">WEBSITE</h5>
                                                </a>
                                            </div>
                                            <div className="Fee">

                                                <a href="https://news.kucoin.com/en/fee/" target="_blank" >
                                                    <h5 className="hover_section">FEE</h5></a>
                                            </div>
                                            <div className="Chat">
                                                <a href="https://news.kucoin.com/en/" target="_blank" >
                                                    <h5 className="hover_section">BLOG</h5></a>
                                            </div>
                                            <div className="compare-button">
                                                <ButtonToolbar>
                                                    <Link to="/Compare"> <Button className="compare-btn" variant="primary">COMPARE</Button></Link>
                                                </ButtonToolbar>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="coinPriceSelection">
                                <div className="row ">
                                    {items.slice(0, 8).map(item => {
                                        const CHANGEPCT24HOUR = (Math.round(item.RAW.USD.CHANGEPCT24HOUR * 100) / 100)
                                        const PRICE1 = (Math.round(item.RAW.USD.PRICE * 100) / 100)
                                        return (
                                            <div className="col-6 col-sm-4 col-md-4 col-lg-2 mt-5 Allcoin_section">
                                                <p><img style={{
                                                    height: "30px"
                                                }} className="coin-image" src={`http://www.cryptocompare.com/${item.CoinInfo.ImageUrl}`} alt={item.CoinInfo.FullName} />{item.CoinInfo.FullName}</p>
                                                <p className="font-coin m-0">{PRICE1} <span className="pl-3">{renderchange(CHANGEPCT24HOUR)}</span> </p>
                                                <p className="font-coin">{item.RAW.USD.TOSYMBOL}</p>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                            <Coins />
                        </>
                    )
                })}
            </ProductConsumer>
        )
    }
}


