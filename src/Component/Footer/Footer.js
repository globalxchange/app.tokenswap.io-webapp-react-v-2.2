import React, { Component } from 'react'
import './Footer.css'
export default class Footer extends Component {
    render() {
        return (
            <>
            <div className="footer-Main">
                <p className="footer-text m-0">Cryptocurrency icons provided by <a className="a-col" href="https://coinmarketcap.com/">CoinMarketCap.com</a></p>
            </div>
             <div className="Sub-footer-Main">
             <p className="Sub-footer-text m-0">Interested in a skillfully crafted product for your business or idea? Contact us at <a className="a-col" href="https://coinmarketcap.com/">2amigos</a></p>
         </div>
         </>
        )
    }
}
