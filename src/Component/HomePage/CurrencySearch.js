import React, { Component } from 'react';
import './CurrencySearch.css'
import bibox from '../../Image/bibox.png'
import binance from '../../Image/binance.png'
import bitfinex from '../../Image/bitfinex.png'
import bittrex from '../../Image/bittrex.png'
import coss from '../../Image/coss.png'
import kraken from '../../Image/kraken.png'
import kucoin from '../../Image/kucoin.png'
import poloniex from '../../Image/poloniex.png'
import upbit from '../../Image/upbit.png'
import { Parallax } from 'react-parallax';

const products = [
  { id: 1, liks: `${"/Exchange/Bibox"}`, imgs: bibox, name: "Bibox", paragraph: "Bibox is a digital asset exchange based on AI technology that provides secure, stable and reliable exchange service for digital assets such as Bitcoin, Ethereum, Litecoin, Ethereum Classic, BIX, NEO, EOS, USDT, GUSD, BCH, Grin.", vistwebsite: `${"https://www.bibox.com/"}`, fees: `${"https://bibox.zendesk.com/hc/en-us/articles/360002336133"}`, chat: `${"https://t.me/biboxglobal"}` },
  { id: 2, liks: `${"/Exchange/Bibox"}`, imgs: binance, name: "Binance", paragraph: "Binance is a cryptocurrency exchange platform started in China but recently moved their headquarters to the crypto-friendly Island of Malta in the EU.", paragraph2: "There are two options for trading platforms on Binance: Basic and Advanced. As the names imply, the Basic view has all the user need to perform simple trades while the Advanced view is for more advanced traders. There aren’t many differences between the two views with the exception of being able to do more detailed technical analysis in the Advanced view.", vistwebsite: `${"https://www.binance.com/en"}`, fees: `${"https://www.binance.com/en/fee/schedule"}`, chat: `${"https://t.me/binanceexchange"}` },
  { id: 3, liks: `${"/Exchange/Bibox"}`, imgs: bitfinex, name: "Bitfinex", paragraph: "BitFinex offers three main functions - it is a bitcoin, litecoin and dashcoin to fiat exchange, a margin trading exchange and a liquidity provider. Although the company is said to be in Beta phase there are a number of features available that expand the financial positions you can take - for example the ability to short Bitcoin via margin trading.", vistwebsite: `${"https://www.bitfinex.com/"}`, fees: `${"https://bibox.zendesk.com/hc/en-us/articles/360002336133"}`, blog: `${"https://t.me/biboxglobal"}` },
  { id: 4, liks: `${"/Exchange/Bibox"}`, imgs: bittrex, name: "Bittrex", paragraph: "Bittrex is one of the larger crypto to crypto exchanges offering a large number of trading pairs into bitcoin. The exchange does have a very high turnover crypto currencies - leading some to accuse it of allowing pump and dump schemes that erode confidence in the crypto currency ecosystem as a whole.", vistwebsite: `${"https://international.bittrex.com"}`, fees: `${"https://bittrex.com/Fees"}`, blog: `${"https://bittrex.zendesk.com/hc/en-us/sections/360000567032-Blog"}` },
  { id: 5, liks: `${"/Exchange/Bibox"}`, imgs: coss, name: "Coss", paragraph: "COSS stands for Crypto-One-Stop-Solution and represents a platform, which encompasses all features of a digital economical system based on cryptocurrency.The COSS system consists of a payment gateway / POS, an exchange, a merchant list, market cap rankings, a marketplace, an e-wallet, various coin facilities and a mobile platform. The COSS platform unifies all transactional aspects that are usually managed by means of FIAT money, and offers multiple cryptocurrency-related services in one place.", vistwebsite: `${"https://www.coss.io/"}`, fees: `${"https://coss.io/c/fees"}`, chat: `${"https://t.me/myCOSS"}` },
  { id: 6, liks: `${"/Exchange/Bibox"}`, imgs: kraken, name: "Kraken", paragraph: "Kraken is a top European based exchange and offers a variety of fiat to bitcoin pairs such as JPY, EUR, GBP and USD. Volume is decent especially on the JPY BTC pair after MT Gox's collapse - with Kraken assuming the mantle in that region. The exchange also has a smattering if popular crypto to crypto pairs including litecoin and dogecoin.", vistwebsite: `${"https://www.kraken.com/"}`, fees: `${"https://www.kraken.com/en-us/help/fees"}`, blog: `${"https://blog.kraken.com/"}` },
  { id: 7, liks: `${"/Exchange/Bibox"}`, imgs: kucoin, name: "kucoin", paragraph: "KCS is an Ethereum-based ERC20 token issued by the Kucoin Cryptocurrency Exchange. The token holders benefit from bonuses (50% of the total trading fees charged by the platform), trading fee discounts, and other special services. Kucoin Cryptocurrency Exchange will buy back 100 million of the 200 million total tokens issued and burned them.", vistwebsite: `${"https://www.kucoin.com/"}`, fees: `${"https://news.kucoin.com/en/fee/"}`, blog: `${"https://news.kucoin.com/en/"}` },
  { id: 8, liks: `${"/Exchange/Bibox"}`, imgs: poloniex, name: " Poloniex", paragraph: "Poloniex is a crypto to crypto exchange with their headquarters located in Wilmington (HQ), DE United States.For a crypto to crypto exchange there is good security and decent volume and orderbook depth for the majority of its trading pairs.", vistwebsite: `${"https://poloniex.com/"}`, fees: `${"https://poloniex.com/fees/"}`, chat: `${"http://www.polonibox.com/"}` },
  { id: 9, liks: `${"/Exchange/Bibox"}`, imgs: upbit, name: "Upbit", paragraph: "UPbit is a multi-cryptocurrency exchange with service centers based in South Korea. UPbit offers trading pairs such as KRW/EOS, BTC/KRW, BTC/BCC, BTC/ETH, ZEC/BTC, XRP/ETH, LTC/KRW, etc. It also provides users with an easy to use mobile app available for iOS and Android.", vistwebsite: `${"https://upbit.com/"}`, fees: `${"https://bibox.zendesk.com/hc/en-us/articles/360002336133"}`, chat: `${"https://t.me/biboxglobal"}` }
]


function searchingFor(term) {
  return function (x) {
    return x.name.toLowerCase().includes(term.toLowerCase()) || false;
  }
}
class CurrencySearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: products,
      term: "",
    }
    this.searchHandler = this.searchHandler.bind(this);
  }
  searchHandler(event) {
    this.setState({ term: event.target.value })
    console.log(event.target.value);
  }
  render() {
    const { term } = this.state;
    return (
      <div className="Search_Section">
        <Parallax
          blur={1}
          bgImage={require('../../Image/bck.jpg')}
          bgImageAlt="image"
          strength={300} >
          <div className="container">
            <div>
              <div className="serach-Context-page">
                <h1>DISCOVER AND COMPARE</h1>
                <p className="crypto_text">Crypto market data in one place</p>
                <form className="Form-Selection">
                  <input className="search-button" type="text" onChange={this.searchHandler} value={term} placeholder="Search Exchanges" />
                  <span><i className="fas-seraching fas fa-search"></i></span>
                </form>
              </div>
            </div>
          </div>
          <div style={{ height: '150px' }} />
        </Parallax>
        <div className="container  Search-pge">
          {products.filter(searchingFor(term)).map((product) =>

            <div className="box-shaddow-search">

              <a href={product.liks}>
                <div key={product.id}>
                  <div className="img-div-section">
                    <img className="c_img" src={product.imgs} />
                    <p className="C-Name">{product.name}</p>
                  </div>
                  <div className="pt-3 pb-3 P-borber">
                    <p>{product.paragraph}</p>
                    <p>{product.paragraph2}</p>
                  </div>
                </div>
              </a>
              <div className="Home_Link_Section">
                <div className="Home_link-SubSection">
                  <a className="line-a" target="_blank" href={product.vistwebsite}>VISIT WEB URL</a>
                </div>
                <div className="Home_link-SubSection">
                  <a className="line-a" target="_blank" href={product.fees}>FEES</a>
                </div>
                <div className="Home_link-SubSection">
                  <a className="line-a" target="_blank" href={product.chat}>CHAT</a>
                  <a className="line-a" target="_blank" href={product.blog}>BLOG</a>
                </div>
              </div>

            </div>
          )
          }
        </div>
      </div>
    );
  }
}

export default CurrencySearch;