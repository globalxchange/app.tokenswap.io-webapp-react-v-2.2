import React, { Component } from 'react';
import bibox from '../Image/bibox.png'
import binance from '../Image/binance.png'
import bitfinex from '../Image/bitfinex.png'
import bittrex from '../Image/bittrex.png'
import coss from '../Image/coss.png'
import kraken from '../Image/kraken.png'
import kucoin from '../Image/kucoin.png'
import  poloniex from '../Image/poloniex.png'
import  upbit from '../Image/upbit.png'        
const ProductContext = React.createContext();


  
class ProductProvider extends Component {
   
       state = {
            items:[],
            check:"",
            isLoaded:false,
            count:false,
            modalIsOpen: false,
            Data:[{  Name:'Bibox',images:bibox},
{Name:'Binance',images:binance},
{Name:'Bitfinex',images:bitfinex},
{Name:'Bittrex',images:bittrex},
{Name:'Coss',images:coss},
{Name:'Kraken',images:kraken},
{Name:'Kucoin',images:kucoin},
{Name:'Poloniex',images:poloniex}, 
{Name:'Upbit',images:upbit},

],
selected:[],
}
    


inputValue = async (item) => {
// console.log(item);
if(this.state.selected.includes(item)){
    let index = this.state.selected.indexOf(item);
     
     this.state.selected.splice(index,1);
    
    // console.log(index)
   await this.setState({
        selected:this.state.selected,
      
        count: this.state.count - 1
    })
}else {
    await this.setState({
        selected:[...this.state.selected,item],
        count: this.state.count + 1
    });
}
console.log("asdasdasdasdasd",this.state.count);
}

 componentDidMount=()=>
 {
    // const rp = require('request-promise');
    // const requestOptions = {
    //     method: 'GET',
    //     uri: 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest',
       
    //     headers: {
    //       'X-CMC_PRO_API_KEY': 'f9259b8b-0fff-4d69-99c0-f1ef87adfbdf'
    //     },
    //     json: true,
    //     gzip: true
    //   };
      
    //   rp(requestOptions).then(response => {
    //     console.log('API call response:', response);
    //   }).catch((err) => {
    //     console.log('API call error:', err.message);
    //   }); 

      fetch("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest", {
        method: 'GET',
  qs: {
    'start': '1',
    'limit': '5000',
    'convert': 'USD'
  },
  json: true,    gzip: true,
  headers: {  
    'X-CMC_PRO_API_KEY': 'f9259b8b-0fff-4d69-99c0-f1ef87adfbdf',
  
  }
})   
  .then(response => response.json())
  .then((responseData) => {
    this.setState({ author: responseData});
  })
  .catch(error => this.setState({ error }));
 }
openModal = () => {
    this.setState({modalIsOpen: true});
  }
 

  closeModal = () => {
    this.setState({modalIsOpen: false});
  }
 
componentDidMount()
{
    this.cryptoFunc();
    
    
}
handSub=(e)=>{
    e.preventDefault();
    const userdata={
   
    check:this.state.check,
    }
    console.log("dsa" ,this.state.check)
    console.table(userdata);
    

    }
cryptoFunc=()=>{
    fetch('https://min-api.cryptocompare.com/data/top/totalvolfull?limit=100&tsym=USD')
.then(res =>res.json())
.then(json =>{
   this.setState({
       isLoaded:true,
       items:json.Data
        })

   });
  
} 

render() {

    // console.log("kamal",this.state.items.CoinInfo.FullName)
return (
<ProductContext.Provider value={{
...this.state,
inputValue:this.inputValue,
openModal:this.openModal,
closeModal:this.closeModal,
afterOpenModal:this.afterOpenModal,
handSub:this.handSub
// searchHandler1:this.searchHandler1,
// searchingFor:this.searchingFor

}}>
{this.props.children}
</ProductContext.Provider>
)

}
}

const ProductConsumer = ProductContext.Consumer;

export { ProductProvider, ProductConsumer };