import React, { Component, createContext } from "react";

export const Hacker = createContext();
export class HackerProvider extends Component {

  state = {
mode:false
          }

          togglemode=async()=>{
              await this.setState({
                  mode:!this.state.mode
              })
       
              if (this.state.mode===true) {
          
                document.documentElement.style.setProperty("--main-color", "#ffff");

                document.documentElement.style.setProperty("--back-color", "#18191D");
                document.documentElement.style.setProperty("--cutombtext", "#18191D");
                document.documentElement.style.setProperty("--next-previuse", "#EBEBEB");
    
                document.documentElement.style.setProperty("--border-color", "#e5e5e5");

              } 
              else{
                document.documentElement.style.setProperty("--main-color", "#464B4E");
                document.documentElement.style.setProperty("--cutombtext", "#18191D");
                document.documentElement.style.setProperty("--next-previuse", "#EBEBEB");
                document.documentElement.style.setProperty("--back-color", "#ffffff");
        
                document.documentElement.style.setProperty("--cutombtext", "#464B4E");
  
                document.documentElement.style.setProperty("--border-color", "#464B4E");
              }
          }
  render() {
    return (
      <Hacker.Provider
        value={{
          ...this.state,
          togglemode:this.togglemode
        }}
      >
        {/* <ContextDevTool context={Agency} id="uniqContextId" displayName="Context Display Name" /> */}
        {this.props.children}
      </Hacker.Provider>
    );
  }
}
