
import React, { Component } from 'react'
import Bibox from './Component/Bibox/Bibox'
import Navigation from './Component/Navbar/Navigationbar'
import HomePages from './Component/HomeDashboard/HomeDashboard'
import Compare from './Component/Compare/Compare'
import Binance from './Component/Binance/Binance'
import Bittrex from './Component/Bittrex/Bittrex'
import Bitfinex from './Component/Bitfinex/Bitfinex'
import Kraken from './Component/Kraken/Kraken'
import Coss from './Component/Coss/Coss'
import Footer from './Component/Footer/Footer'
import Kucoin from './Component/Kucoin/Kucoin'
import Upbit from './Component/Upbit/Upbit'
import Poloniex from './Component/Poloniex/Poloniex'
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'
export default class App extends Component 
{
  render() {
    return (
      <div>
      <Router>

        <Switch>
          <Route exact  path="/" component={HomePages}/>
          <Route  path="/Exchange/Bibox" component={Bibox}/>
          <Route  path="/Exchange/Binance" component={Binance}/>
          <Route  path="/Exchange/Bittrex" component={Bittrex }/>
          <Route  path="/Exchange/Bitfinex" component={Bitfinex}/>
          <Route  path="/Exchange/Coss" component={Coss}/>
          <Route  path="/Exchange/Kraken" component={Kraken}/>
          <Route  path="/Exchange/Kucoin" component={Kucoin}/>
          <Route  path="/Exchange/Poloniex" component={Poloniex}/>
          <Route  path="/Exchange/Upbit" component={Upbit}/>
          <Route  path="/Compare" component={Compare}/>
          </Switch>
     
        </Router>
        </div>
    )
  }
}

