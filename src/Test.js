import React from 'react'

import {Mycontext} from './Context'
export default (props)=>
{return(
 
    <Mycontext.Consumer>
        {(context)=>{
            return(
                <>
<h1>test{context.cont}</h1>
<button onClick={context.increment}>icm</button>
<button onClick={context.decrement}>fd</button>
</>
            )
        }
        }
          {items.map(item=>{
                           const products = [ {
                            Currency:item.CoinInfo.FullName,
                            Marketpair:item.CoinInfo.Internal+"/"+item.RAW.USD.TOSYMBOL ,
                            price:234
                        } ];
                const columns = [{
                  dataField: 'Currency',
                  text: 'Currency'
                }, {
                  dataField: 'Marketpair',
                  text: 'Market Pair'
                }, {
                  dataField: 'price',
                  text: 'Product Price'
                }];
                const expandRow = {
                  renderer: row => (
                    <div>
                      <p>{ `This Expand row is belong to rowKey ${row.id}` }</p>
                      <p>You can render anything here, also you can add additional data on every row object</p>
                      <p>expandRow.renderer callback will pass the origin row object to you</p>
                    </div>
                  )
                };
</Mycontext.Consumer>

)
}


import React, { Component } from 'react'
import { ProductConsumer } from '../ContextApi/ContextApi'
import './Coins.css'
import { Table } from 'react-bootstrap';


function searchingFor(term) {
  return function (x) {
    //   return x.name.toLowerCase().includes(term.toLowerCase()) || false;
    // }
  }
}
export default class Coins extends Component {
  constructor(props) {
    super(props);
    this.state = {

      term: "",
    }
    this.searchHandler = this.searchHandler.bind(this);
  }
  searchHandler(event) {
    this.setState({ term: event.target.value })
    console.log(event.target.value);
  }

  render() {
    
    return (
      <ProductConsumer>
        
        {((value) => {

          const { items } = value;

          // console.log("kmal",items.FullName);
          return (

            <div>
            
              <Table responsive>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Table heading</th>
                    <th>gable heading</th>
                    <th>Table heading</th>
                    <th>Table heading</th>
                    <th>Table heading</th>
                    <th>Table heading</th>
                  </tr>
                </thead>
                <tbody>
                  {items.map(item => {
                    return (
                      <tr>
                        <td>1</td>
                        <td>{item.FullName}</td>
                        <td>Table cell</td>
                        <td>Table cell</td>
                        <td>Table cell</td>
                        <td>Table cell</td>
                        <td>Table cell</td>
                      </tr>
                    )
                  })}
                </tbody>
              </Table>


            </div>
          )
        })}

      </ProductConsumer>


    )
  }
}